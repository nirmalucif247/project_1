from django.shortcuts import render
from django.views.generic import TemplateView, View
from .models import *
from django.db.models import Avg, Count, Min, Sum
import json

class HomeView(TemplateView):
    template_name = 'home.html'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["stud"] = Student.objects.all()
        return context

class ExpenseView(TemplateView):
    template_name = 'expense.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        expense = Expense.objects.all().order_by('-id')
        context["exp"] = expense

        total = 0
        for ex in expense:
            total += ex.amount
        context['total'] = total
        students  = Student.objects.all()[:4]
        label = []
        # label = ''
        data= []
        for s in students:
            # label += s.name + ","
            label.append(s.name)

            total_amount = Expense.objects.filter(student__name=s.name).aggregate(total=Sum('amount'))['total']
            data.append(total_amount)
        context['name'] = label
        # context['name1'] = label[0]
        # context['name2'] = label[1]
        # context['name3'] = label[2]
        # context['name4'] = label[3]

        context['data'] = list(data)



    
        return context

class StudentDetailView(TemplateView):
    template_name = 'expensedetail.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        stud_id = self.kwargs['s_id']
        stud_obj = Student.objects.get(id = stud_id)
        context['student'] = stud_obj


        total = 0
        expense = Expense.objects.filter(student=stud_obj)
        for ex in expense:
            total += ex.amount
        context['total'] = total


        return context

    
    
    
