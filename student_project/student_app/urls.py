from django.urls import path
from .views import *

app_name = 'student_app'
urlpatterns = [
    path('', HomeView.as_view(), name = 'home'),
    path('expenses/', ExpenseView.as_view(), name = 'expense'),
    path("student-detail/<int:s_id>/", StudentDetailView.as_view(), name = 'student_detail'),

]
